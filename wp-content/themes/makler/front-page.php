<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package makler
 */

get_header(); ?>

    <main>
        <?php get_template_part('slider') ?>

        <div class="container">
            <div class="links-group row">
                <div class="col-sm-6 col-md-4"><a href="<?= get_category_link( '3' ); ?>"
                                                  class="category insurance"><?php esc_html_e('Versicherungen', 'makler') ?></a>
                </div>
                <div class="col-sm-6 col-md-4"><a href="<?= get_category_link( '9' ); ?>"
                                                  class="category bank"><?php esc_html_e('Bankprodukte', 'makler') ?></a>
                </div>
                <div class="col-sm-6 col-md-4"><a href="<?= get_category_link( '10' ); ?>"
                                                  class="category strom"><?php esc_html_e('Strom, Gas & DSL', 'makler') ?></a>
                </div>
                <div class="col-sm-6 col-md-4"><a href="<?= get_category_link( '4' ); ?>"
                                                  class="category construct"><?php esc_html_e('Baufinanzierung', 'makler') ?></a>
                </div>
                <div class="col-sm-6 col-md-4"><a href="<?= get_category_link( '7' ); ?>"
                                                  class="category property"><?php esc_html_e('Immobilien', 'makler') ?></a>
                </div>
                <div class="col-sm-6 col-md-4"><a href="<?= get_category_link( '8' ); ?>"
                                                  class="category metals"><?php esc_html_e('Edelmetalle', 'makler') ?></a>
                </div>
                <div class="col-xs-12">
                    <hr class="separate">
                </div>
            </div>
            <div class="container">
             <h2 align="center">Versicherungen online berechnen</h2><br><br>
            <div class="links-group second-links-group row">
                <div class="col-sm-6 col-md-4"><a href="https://www.vhv.de/vhv/tarifrechner-pkw.htm?standalone=1&iVMNr=161539-000" class="btn4" target="_blank">KFZ</a></div>
                <div class="col-sm-6 col-md-4"><a href="https://lotse.softfair-server.de/b2c/#/bedarfsermittlung?userexid=218b88ea-91e4-11e5-8000-e87944671d4a" class="btn4" target="_blank">Bedarfsermittlung</a></div>
                <div class="col-sm-6 col-md-4"><a href="	<?php echo get_option('home'); ?>/versicherungen/haftpflichtversicherung/privathaftpflicht/" class="btn4">PHV</a></div>
                <div class="col-sm-6 col-md-4"><a href="<?php echo get_option('home'); ?>/versicherungen/haus-und-wohnen/hausrat-glasversicherung/" class="btn4">Hausrat</a></div>
                <div class="col-sm-6 col-md-4"><a href="<?php echo get_option('home'); ?>/versicherungen/haftpflichtversicherung/tierhalterhaftpflicht/" class="btn4">Hundehalterhaftpflicht</a></div>
                <div class="col-sm-6 col-md-4"><a href="<?php echo get_option('home'); ?>/versicherungen/haftpflichtversicherung/tierhalterhaftpflicht/" class="btn4">Pferdehalterhaftpflicht</a></div>
                <div class="col-sm-6 col-md-4"><a href="<?php echo get_option('home'); ?>/versicherungen/familie-und-freizeit/unfallversicherung/" class="btn4">Unfall</a></div>
                <div class="col-sm-6 col-md-4"><a href="<?php echo get_option('home'); ?>/versicherungen/rechtsschutzversicherung/" class="btn4">Rechtsschutz</a></div>
                <div class="col-sm-6 col-md-4"><a href="https://lotse.softfair-server.de/b2c/#/ergebnis/ergaenzung?userexid=218b88ea-91e4-11e5-8000-e87944671d4a" class="btn4" target="_blank">Private Krankenversicherung</a></div>
                <div class="col-sm-6 col-md-4"><a href="https://lotse.softfair-server.de/b2c/#/ergebnis/ergaenzung/0/ambulant?userexid=218b88ea-91e4-11e5-8000-e87944671d4a" class="btn4" target="_blank">Ambulante Zusatzversicherung</a></div>
                <div class="col-sm-6 col-md-4"><a href="https://lotse.softfair-server.de/b2c/#/ergebnis/ergaenzung/0/dental?userexid=218b88ea-91e4-11e5-8000-e87944671d4a" class="btn4" target="_blank">Dentale Zusatzversicherung</a></div>
                <div class="col-sm-6 col-md-4"><a href="https://lotse.softfair-server.de/b2c/#/ergebnis/ergaenzung/0/stationaer?userexid=218b88ea-91e4-11e5-8000-e87944671d4a" class="btn4" target="_blank">Stationäre Zusatzversicherung</a></div>
            </div>
            </div>
            <!--/.row-->
            <?php get_template_part('latest-articles'); ?>
        </div>

        <!--/.container-->

        <div class="honors">
            <?php echo bxslider_template(); ?>
        </div>
        <!--/.honors-->

        <section class="last-reviews container">
            <div class="row">
                <div class="col-xs-12">
                    <header>
                        <h2><?php esc_html_e('Bewertungen', 'makler'); ?></h2>
                    </header>
                    <?php // Query Arguments
                    $args = array(
                        'post_type' => 'reviews',
                        'posts_per_page' => 3
                    );
                    // The Query
                    $the_query = new WP_Query($args);
                    while ($the_query->have_posts()) : $the_query->the_post(); ?>
                        <article class="last-review">
                            <figure>
                                <p class="wreper-img">
                                    <?php the_post_thumbnail(array('133','133'), array(
                                        'class' => 'img-responsive',
                                        'alt' => trim(strip_tags(get_the_title()))
                                    )); ?>
                                </p>
                                <figcaption>
                                    <header>
                                        <h3><?php the_title(); ?></h3>
                                    </header>
                                    <?php the_content(); ?>
                                </figcaption>
                            </figure>
                        </article>
                        <!--/.last-review-->
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>
                    <div class="clear"></div>
                    <!--<footer>
                        <p><a href="`" class="btn2">Alle Bewertungen</a></p>
                    </footer>-->
                    <hr class="separate">
                </div>
            </div>
        </section>
        <!--/.last-reviews-->

        <section class="additionl-info container">
            <div class="row">
                <div class="col-xs-12">
                    <?php the_content(); ?>
                </div>
            </div>
        </section>
        <!--/.adiitinal-info-->
    </main>

<?php
get_footer();
