<?php
/*
Template Name: Subcategory
*/

?>

<? get_header(); ?>

    <div id="primary" class="content-area">
    <div class="main" role="main">
        <?php /*get_template_part('slider')*/ ?>
        <div class="container">
            <div class="row">
                <?php if(have_posts()) : the_post(); ?>
                <div class="category-content col-xs-12">
                    <header class="page-header">
                        <h1><?php the_title(); ?></h1>
                    </header><!-- .page-header -->

                    <div class="content-category"><?php the_content(); ?></div>

                    <hr class="separate">

                    <div class="links-group links-group-category row">

                        <?php
                        $cat_id = get_field('category_id');
                        $args_category = array(
                            'parent' => $cat_id,
                            'hide_empty' => false
                        );
                        $categories = get_categories($args_category);
                        foreach ($categories as $category) { ?>

                            <!--<div class="col-sm-6 col-md-4"><a href="<?php /*echo get_category_link($category->cat_ID) */ ?>"
                                                              class="category"><?php /*esc_html_e($category->name, 'makler') */ ?></a>
                            </div>-->
                            <artilce class="latest-article">
                                <figure>
                                    <p class="wreper-img"><a href="<?php echo get_category_link($category->cat_ID) ?>">
                                            <?php
                                            if ($imgcat = get_field("imgcat", $category)) : ?>
                                                <img src="<?php echo $imgcat;?>" height="160" width="160"/>
                                            <?php else:
                                                echo '<img src="' . get_template_directory_uri() . '/img/no_photo1.jpg" alt="No image" class="img-responsive" height="160" width="160">';
                                            endif; ?>
                                        </a></p>
                                    <figcaption>
                                        <header>
                                            <h3>
                                                <a href="<?php echo get_category_link($category->cat_ID) ?>"><?php esc_html_e($category->name, 'makler') ?></a>
                                            </h3>
                                        </header>
                                        <?php
                                        if (category_description($category->cat_ID))
                                        echo mb_substr(category_description($category->cat_ID), 0, 300);
                                         ?>

                                    </figcaption>
                                </figure>
                            </artilce>
                            <!--/.latest-article-->
                        <?php } ?>

                    </div>
                    <!--/.row-->

                </div>
                <?php endif; ?>
            </div>


        </div><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();
