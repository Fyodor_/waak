<?php
/*
Template Name: Nrv-product
*/
get_header('nrv-product');
?>

<section class="slider product-slide nrv">
      <div class="slider-wrapper">
        <div class="slide" style="background-image: url(<?php echo get_the_post_thumbnail_url($id, 'full'); ?>)">
          <div class="container-new">
            <div class="price-wrapper">
              <div class="icon docura-icon-slider-leaf">
                <div class="price-title-1"><?php the_field('slider-text-1'); ?></div>
                <div class="price"><?php the_field('slider_price'); ?><span> EUR</span></div>
                <div class="price-title-2"><?php the_field('slider-text-2'); ?></div>
              </div>
            </div>
            <h4><?php echo get_the_title(); ?></h4>
            <div class="slider-text">
              <p><?php echo get_the_content(); ?></p>
            </div>
            <div class="sertifficate-wrapper sertifficate-wrapper-small">
              <?php
                  $sertificates = carbon_get_post_meta( get_the_ID(), 'sertificate' );
                  if ( $sertificates ) {
                      foreach ( $sertificates as $sertificate ) {
                        ?>
                        <?php 
                          $link = $sertificate['sertificate'];
                          echo wp_get_attachment_image( $link , 'large'); 
                        ?>
                        <?php
                      }
                  }
                ?>
            </div><a class="btn btn-primary-docura btn-primary-nrv" href="#calculator">Jetzt online abschließen</a>
            <div class="sertifficate-wrapper" style="color:#fff;">
              <?php
                $sertificates = carbon_get_post_meta( get_the_ID(), 'sertificate' );
                if ( $sertificates ) {
                    foreach ( $sertificates as $sertificate ) {
                      ?>
                      <?php 
                        $link = $sertificate['sertificate'];
                        echo wp_get_attachment_image( $link , 'full'); 
                      ?>
                      <?php
                    }
                }
              ?>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="product-vorteile" id="vorteile">
      <div class="container-new">
        <h2 class="h2-docura"><?php echo carbon_get_post_meta( get_the_ID(), 'vorteile_header' ); ?></h2>
        <div class="product-vorteile-content-wrapper">
        <ul>
        <?php
          $vorteiles = carbon_get_post_meta( get_the_ID(), 'vorteile' );
          if ( $vorteiles ) {
              foreach ( $vorteiles as $vorteile ) {
                ?>
                <li><?php echo $vorteile['vorteile']; ?></li>
                <?php
                  
              }
          }
        ?>
          </ul>

          <?php
          $vorteiles = carbon_get_post_meta( get_the_ID(), 'vorteile_files' );
          if ( $vorteiles ) {
              foreach ( $vorteiles as $vorteile ) {
                ?>
                <?php 
                $link = $vorteile['vorteile_file'];
                $text = $vorteile['vorteile_file_name'];
                echo wp_get_attachment_link( $link, '', false, false, $text); 
                ?>
                <?php
              }
          }
        ?>
          <!-- <a href="#">Mehr details (PDF) zur Domcura Privathaftpflichtversicherung</a> -->
        </div>
      </div>
    </section>
    <section class="step nrv" id="wieman1">
      <div class="container-new">
        <h2 class="h2-docura"><?php echo carbon_get_post_meta(get_the_ID(), 'nur_header'); ?></h2>
        <div class="step-content-wrapper">
          <div class="subheader"><?php echo carbon_get_post_meta(get_the_ID(), 'nur_sub_header'); ?></div>
          <div class="step-items">
            <div class="step-item">
              <div class="img-wrapper"><?php 
              $link = carbon_get_post_meta( get_the_ID(), 'nur-image-1' );
              echo wp_get_attachment_image( $link ); 
              ?></div>
              <div class="text-wrapper">
                <p><?php echo carbon_get_post_meta( get_the_ID(), 'nur-text-1' ); ?></p>
              </div>
            </div>
            <div class="step-item">
              <div class="img-wrapper"><?php 
              $link = carbon_get_post_meta( get_the_ID(), 'nur-image-2' );
              echo wp_get_attachment_image( $link ); 
              ?></div>
              <div class="text-wrapper">
                <p><?php echo carbon_get_post_meta( get_the_ID(), 'nur-text-2' ); ?></p>
              </div>
            </div>
            <div class="step-item">
              <div class="img-wrapper"><?php 
              $link = carbon_get_post_meta( get_the_ID(), 'nur-image-3' );
              echo wp_get_attachment_image( $link ); 
              ?></div>
              <div class="text-wrapper">
                <p><?php echo carbon_get_post_meta( get_the_ID(), 'nur-text-3' ); ?></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="product-why" id="why">
      <div class="container-new">
        <h2 class="h2-docura"><?php echo carbon_get_post_meta(get_the_ID(), 'warum_header'); ?></h2>
        <div class="product-content-wrapper">
          <p><?php echo carbon_get_post_meta( get_the_ID(), 'warum-text' ); ?></p>
        </div>
      </div>
    </section>

    <section class="calculator" id="calculator">
      <div class="container-new">
        <h2 class="h2-docura"><?php echo carbon_get_post_meta(get_the_ID(), 'header-calc'); ?></h2>
        <div class="calculator-wrapper">
          <div class="small-calc">
            <?php echo carbon_get_post_meta( get_the_ID(), 'small-calc' ); ?>
          </div>
          <div class="big-calc-src" style="display:none;">
            <?php echo carbon_get_post_meta( get_the_ID(), 'big-calc' ); ?>
          </div>
          
          <!-- <iframe id="mrmoFrame" name="mrmoFrame" scrolling="no" border="0" style="border:0; width: 1px;min-width: 100%;*width: 100%;" src="https://www.mr-money.de/cookievgl.php?sp=phv&amp;ur_iD=mobile&amp;id=00206831" width="100%"></iframe> -->
        </div>
      </div>
    </section>

    <section class="falle nrv">
      <div class="container-new">
        <h2 class="h2-docura"><?php echo carbon_get_post_meta(get_the_ID(), 'in_diesen-header'); ?></h2>
        <div class="panel-group accordions-all" id="accordion" role="tablist" aria-multiselectable="true">
          
            <?php
              $in_diesens = carbon_get_post_meta( get_the_ID(), 'in_diesen' );
              if ( $in_diesens ) {
                  foreach ( $in_diesens as $in_diesen ) {
                    ?>
                      <div class="panel panel-default">
                        <div class="panel-heading" id="heading<?php echo $in_diesen['in_diesen_number']; ?>" role="tab">
                          <h4 class="panel-title" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $in_diesen['in_diesen_number']; ?>" aria-expanded="true" aria-controls="collapse<?php echo $in_diesen['in_diesen_number']; ?>"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $in_diesen['in_diesen_number']; ?>" aria-expanded="true" aria-controls="collapse<?php echo $in_diesen['in_diesen_number']; ?>"><?php echo $in_diesen['in_diesen_header']; ?></a></h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapse<?php echo $in_diesen['in_diesen_number']; ?>" role="tabpanel" aria-labelledby="heading<?php echo $in_diesen['in_diesen_number']; ?>">
                          <div class="panel-body">
                            <?php
                              echo $in_diesen['in_diesen_text']; 
                            ?>
                          </div>
                        </div>
                      </div>
                    <?php
                  }
              }
            ?>
        </div>
      </div>
    </section>

    <section class="falle nrv" id="falle">
      <div class="container-new">
        <h2 class="h2-docura"><?php echo carbon_get_post_meta( get_the_ID(), 'optionale-header' ); ?></h2>
        <div class="subheader"><?php echo carbon_get_post_meta( get_the_ID(), 'optionale-subheader' ); ?></div>
        <div class="panel-group accordions-all" id="accordion2" role="tablist" aria-multiselectable="true">
        <?php
              $optionales = carbon_get_post_meta( get_the_ID(), 'optionale' );
              if ( $optionales ) {
                  foreach ( $optionales as $optionale ) {
                    ?>
                      <div class="panel panel-default">
                        <div class="panel-heading" id="heading2<?php echo $optionale['optionale_number']; ?>" role="tab">
                          <h4 class="panel-title" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse2<?php echo $optionale['optionale_number']; ?>" aria-expanded="true" aria-controls="collapse2<?php echo $optionale['optionale_number']; ?>"><a role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse2<?php echo $optionale['optionale_number']; ?>" aria-expanded="true" aria-controls="collapse2<?php echo $optionale['optionale_number']; ?>"><?php echo $optionale['optionale_header']; ?></a></h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapse2<?php echo $optionale['optionale_number']; ?>" role="tabpanel" aria-labelledby="heading2<?php echo $optionale['optionale_number']; ?>">
                          <div class="panel-body">
                            <?php
                              echo $optionale['optionale_text']; 
                            ?>
                          </div>
                        </div>
                      </div>
                    <?php
                  }
              }
            ?>
        </div>
      </div>
    </section>

    <section class="falle nrv" id="faq">
      <div class="container-new">
        <h2 class="h2-docura"><?php echo carbon_get_post_meta(get_the_ID(),'haufig-header'); ?></h2>
        <div class="panel-group accordions-all" id="accordion3" role="tablist" aria-multiselectable="true">
        
        <?php
              $haufigs = carbon_get_post_meta( get_the_ID(), 'haufig' );
              if ( $haufigs ) {
                  foreach ( $haufigs as $haufig ) {
                    ?>
                      <div class="panel panel-default">
                        <div class="panel-heading" id="heading3<?php echo $haufig['haufig_number']; ?>" role="tab">
                          <h4 class="panel-title" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapse3<?php echo $haufig['haufig_number']; ?>" aria-expanded="true" aria-controls="collapse3<?php echo $haufig['haufig_number']; ?>"><a role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapse3<?php echo $haufig['haufig_number']; ?>" aria-expanded="true" aria-controls="collapse3<?php echo $haufig['haufig_number']; ?>"><?php echo $haufig['haufig_header']; ?></a></h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapse3<?php echo $haufig['haufig_number']; ?>" role="tabpanel" aria-labelledby="heading3<?php echo $haufig['haufig_number']; ?>">
                          <div class="panel-body">
                            <?php
                              echo $haufig['haufig_text']; 
                            ?>
                          </div>
                        </div>
                      </div>
                    <?php
                  }
              }
            ?>
        </div>
      </div>
    </section>








  
<?php
// параметры по умолчанию
// $args = array(
// 	'numberposts' => 5,
// 	'category'    => 96,
// 	'orderby'     => 'date',
// 	'order'       => 'DESC',
// 	'include'     => array(),
// 	'exclude'     => array(),
// 	'meta_key'    => '',
// 	'meta_value'  =>'',
// 	'post_type'   => 'post',
// 	'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
//);

//$posts = get_posts( $args );

// foreach($posts as $post){ setup_postdata($post);
//   echo '<h2>'.get_the_title().'</h2>';
//   echo '<p>'.get_the_excerpt().'</p>';
// }

// wp_reset_postdata(); // сброс
?>
<?php get_footer('nrv-product'); ?>
