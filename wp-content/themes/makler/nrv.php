<?php
/*
Template Name: Nrv
*/
get_header('nrv');
?>

    <section class="slider nrv">
      <div class="slider-wrapper">

        <?php
        $stati_children = new WP_Query(array(
          'post_type' => 'page',
          'post_parent' => '1286'
          )
        );
        if($stati_children->have_posts()) :
          while($stati_children->have_posts()): $stati_children->the_post();
          ?>
          <div class="slide" style="background-image: url(<?php echo get_the_post_thumbnail_url($id, 'full'); ?>)">
              <div class="container-new">
                <div class="price-wrapper">
                  <div class="icon docura-icon-slider-leaf">
                    <div class="price-title-1"><?php the_field('slider-text-1'); ?></div>
                    <div class="price"><?php the_field('slider_price'); ?><span> EUR</span></div>
                    <div class="price-title-2"><?php the_field('slider-text-2'); ?></div>
                  </div>
                </div>
                <h4><?php echo get_the_title(); ?></h4>
                <div class="slider-text">
                  <p><?php echo get_the_content(); ?></p>
                </div><a class="btn btn-primary-docura btn-primary-nrv" href="<?php echo get_the_permalink(); ?>">Jetzt online abschließen</a>
              </div>
            </div>
          <?php
          endwhile;
        endif; wp_reset_query();
        ?>
      </div>
      <div class="container-new">
        <div class="dots-wrapper"></div>
      </div>
      <div class="container-new">
        <div class="arrows-wrapper"></div>
      </div>
    </section>
    <!-- slider end -->

    <section class="advantages" id="vorteile">
      <div class="container-new">
        <h2 class="h2-docura"><?php
              $post_id = get_post( 1299, ARRAY_A);
              $content = $post_id['post_content'];
              echo ($content);
              ?></h2>
        <div class="advantages__items">
          <div class="advantages__item">
            <div class="advantages__img rellax" data-rellax-speed="-0.3"><img src="<?php the_field('image-1', '1299'); ?>" alt="kundenzufriedenheit"></div>
            <div class="advantages__text-wrapper">
              <h6 class="h6-docura"><?php echo get_post_meta('1299', 'header-1', true); ?></h6>
              <div class="advantages__text"><?php echo get_post_meta('1299', 'text-1', true); ?></div>
            </div>
          </div>
          <div class="advantages__item">
            <div class="advantages__img rellax" data-rellax-speed="-0.3"><img src="<?php the_field('image-2', '1299'); ?>" alt="time"></div>
            <div class="advantages__text-wrapper">
              <h6 class="h6-docura"><?php echo get_post_meta('1299', 'header-2', true); ?></h6>
              <div class="advantages__text"><?php echo get_post_meta('1299', 'text-2', true); ?></div>
            </div>
          </div>
          <div class="advantages__item">
            <div class="advantages__img rellax" data-rellax-speed="-0.3"><img src="<?php the_field('image-3', '1299'); ?>" alt="lock"></div>
            <div class="advantages__text-wrapper">
              <h6 class="h6-docura"><?php echo get_post_meta('1299', 'header-3', true); ?></h6>
              <div class="advantages__text"><?php echo get_post_meta('1299', 'text-3', true); ?></div>
            </div>
          </div>
          <div class="advantages__item">
            <div class="advantages__img rellax" data-rellax-speed="-0.3"><img src="<?php the_field('image-4', '1299'); ?>" alt="hend"></div>
            <div class="advantages__text-wrapper">
              <h6 class="h6-docura"><?php echo get_post_meta('1299', 'header-4', true); ?></h6>
              <div class="advantages__text"><?php echo get_post_meta('1299', 'text-4', true); ?></div>
            </div>
          </div>
        </div>
        <hr>
      </div>
    </section>
    <!-- advantages end -->
    <section class="products" id="produkte">
      <div class="container-new">
        <h2 class="h2-docura">Berechnen Sie die Kosten der Versicherung und kaufen Sie online</h2>
        <div class="products__items">
        <?php
        $stati_children = new WP_Query(array(
          'post_type' => 'page',
          'post_parent' => '1286'
          )
        );
        if($stati_children->have_posts()) :
          while($stati_children->have_posts()): $stati_children->the_post();
          ?>
          <a class="products__item nrv" href="<?php echo get_the_permalink(); ?>" style="background-image: url(<?php echo get_the_post_thumbnail_url($id, 'large'); ?>)">
            <p class="products__header"><?php echo get_the_title(); ?></p>
          </a>


          <?php
          endwhile;
        endif; wp_reset_query();
        ?>
        </div>
        <hr>
      </div>
    </section>
    <!-- products end -->

    <section class="info nrv" id="furkunden">
      <div class="container-new">
        <h2 class="h2-docura"><?php
              $post_id = get_post( 1430, ARRAY_A);
              $content = $post_id['post_content'];
              echo ($content);
              ?></h2>
        <div class="panel-group accordions-all" id="accordion" role="tablist" aria-multiselectable="true">
        <?php
        $stati_children = new WP_Query(array(
          'post_type' => 'post',
          'category_name' => 'nrv-fur'
          )
        );
        if($stati_children->have_posts()) :
          while($stati_children->have_posts()): $stati_children->the_post();
          ?>
          
            <div class="panel panel-default">
            <div class="panel-heading" id="heading<?php echo $id ?>" role="tab">
              <h4 class="panel-title" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $id ?>" aria-expanded="true" aria-controls="collapse<?php echo $id ?>"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $id ?>" aria-expanded="true" aria-controls="collapse<?php echo $id ?>"><?php echo get_the_title(); ?></a></h4>
            </div>
            <div class="panel-collapse collapse" id="collapse<?php echo $id ?>" role="tabpanel" aria-labelledby="heading<?php echo $id ?>">
              <div class="panel-body"><?php echo get_the_content(); ?></div>
            </div>
          </div>

          <?php
          endwhile;
        endif; wp_reset_query();
        ?>
        </div>
      </div>
    </section>
    <!-- info end -->

<?php get_footer('nrv'); ?>