jQuery(document).ready(function ($) {

    if ($('#map').length > 0) {
        function initMap() {
            var myLatLng = {lat: 50.912124, lng: 6.967765};
            var mapDiv = document.getElementById('map');
            var map = new google.maps.Map(mapDiv, {
                center: myLatLng,
                zoom: 17,
                mapMaker: true
            });

            var title = "Koblenzer Straße 88";

            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: title
            });

            /*var marker2 = new google.maps.Marker({
             map: map,
             position: myLatLng,
             title: "",
             icon: {
             url: './img/addedImg/marker2.png',
             anchor: new google.maps.Point(2, 4)
             }
             });*/
        }
    }

})