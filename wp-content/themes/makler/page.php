<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package makler
 */

get_header(); ?>

	<div class="main">
		<?php /*get_template_part( 'slider' )*/ ?>

		<div class="container addedContainer">
			<div class="addedContainerInner">
				<div class="breadcrumbs">
					<?php
					if(function_exists('bcn_display'))
					{
						bcn_display();
					}
					?>
				</div>
				<div class="building">
					<?php
					while ( have_posts() ) : the_post();

						get_template_part( 'template-parts/content', 'page' );

					endwhile; // End of the loop.
					?>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<?php get_template_part('latest-articles'); ?>
			</div>
		</div>


	</div>


<?php
get_footer();
