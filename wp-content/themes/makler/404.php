<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package makler
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<section class="no-results not-found">
							<header class="page-header">
								<h1 class="page-title"><?php esc_html_e('Nothing Found', 'makler'); ?></h1>
							</header><!-- .page-header -->

							<div class="page-content">
								<?php
								if (is_home() && current_user_can('publish_posts')) : ?>

									<p><?php printf(wp_kses(__('Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'makler'), array('a' => array('href' => array()))), esc_url(admin_url('post-new.php'))); ?></p>

								<?php elseif (is_search()) : ?>

									<p><?php esc_html_e('Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'makler'); ?></p>
									<?php
									get_search_form();

								else : ?>

									<p><?php esc_html_e('It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'makler'); ?></p>
									<?php
									get_search_form();

								endif; ?>
							</div><!-- .page-content -->
						</section><!-- .no-results -->
					</div>
				</div>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
