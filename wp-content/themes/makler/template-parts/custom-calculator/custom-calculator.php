<div class="calculator-subheader">
  <?php echo carbon_get_post_meta(get_the_ID(), 'subheader-calc'); ?>
</div>
<?php
  echo do_shortcode( '[contact-form-7 id="1722" title="Custom calculator"]' );
  ?>
<div class="calculator-form-popup hidden visuallyhidden">
Vielen Dank für Ihre Anfrage! Wir werden in Kürze mit Ihnen Kontakt aufnehmen
<img src="<?php bloginfo('template_url'); ?>/template-parts/custom-calculator/img/close-white@3x.png" alt="close button">
</div>
<!-- <button id="btn-tooltip" type="button" class="btn btn-default" data-toggle="tooltip" data-placement="left" title="Tooltip on left">id="btn-tooltip"</button> -->


<?php
?>