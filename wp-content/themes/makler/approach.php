<?php
/*
Template Name: Approach
*/
get_header();
?>

    <div class="main">
        <?php /*get_template_part( 'slider' )*/ ?>

        <div class="container addedContainer">
            <div class="addedContainerInner">
                <div class="breadcrumbs">
                    <?php
                    if(function_exists('bcn_display'))
                    {
                        bcn_display();
                    }
                    ?>
                </div>
                <div class="approach">
                    <h2><?php the_title(); ?></h2>
                    <?php
                    while ( have_posts() ) : the_post();

                        the_content();

                    endwhile; // End of the loop.
                    ?>
                    <div id="address" class="addressWrap" data-lat="<?php the_field('center_latitude'); ?>" data-lng="<?php the_field('center_longitude'); ?>" data-address="<?php the_field('approach'); ?>">
                    <span><?php the_field('approach');  ?></span>
                    </div>
                    <div id="map">

                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
    function initMap() {
        var elem = document.getElementById('address'),
            lat_var = parseInt(elem.getAttribute('data-lat')) ,
            lng_var = parseInt(elem.getAttribute('data-lng'));
        var myLatLng = {lat: lat_var, lng: lng_var};
        var mapDiv = document.getElementById('map');
        var map = new google.maps.Map(mapDiv, {
            center: myLatLng,
            zoom: 16,
            mapMaker: true
        });

        var title = "Koblenzer Straße 88";

        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: title
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?callback=initMap" async defer></script>
<?php get_footer(); ?>

