<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package makler
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <meta name="google-site-verification" content="kYhJ-XqYxpQm0BjqPsD6S-GiL9b1cVJeIGfyu4f8vZE" />

    <?php wp_head(); ?>
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-104874416-1', 'auto');
    ga('send', 'pageview');
    </script>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

    <header class="main-header">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-8 col-sm-6 col-lg-5">
                                <?php
                                if (is_front_page() && is_home()) : ?>
                                    <h1 class="site-title"><a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a></h1>
                                <?php else : ?>
                                    <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>"><?php /*bloginfo('name'); */?>
                                        <img src="<?= get_template_directory_uri().'/img/logo.png'; ?>" alt="<?php bloginfo('name'); ?>"><p>Versicherungen &#x2022; Immobilien
                                            <br> Finanzen</p></a>
                                    <?php
                                endif; ?>

                            </div>
                            <div class="contact-info col-xs-8 col-sm-4 col-lg-5">
                                <span class="tel">0221 - 9937 9931</span>
                                <span class="label">Telefonische Expertenberatung</span>
                            </div>
                            <div class="skype-wreper col-xs-4 col-sm-2">
                                <a href="skype:cashcheck24?call"><i class="skype"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <?php /*wp_nav_menu(array('theme_location' => 'primary', 'menu_id' => 'primary-menu'));*/ ?>
                                <?php
                                $args = array(
                                    'theme_location'            => 'primary',
                                    'menu_class'      => 'main-nav nav navbar-nav',
                                    'depth'           => 0
                                );
                                wp_nav_menu( $args );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
    </header>
    <!-- main-header -->

    <div id="content" class="site-content">
