<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package makler
 */

get_header(); ?>

    <main>
        <?php get_template_part( 'slider' ) ?>

        <div class="container">
            <div class="links-group row">
                <div class="col-sm-6 col-md-4"><a href="" class="category insurance">Versicherungen</a></div>
                <div class="col-sm-6 col-md-4"><a href="" class="category bank">Bankprodukte</a></div>
                <div class="col-sm-6 col-md-4"><a href="" class="category strom">Strom, Gas & DSL</a></div>
                <div class="col-sm-6 col-md-4"><a href="" class="category construct">Baufinanzierung</a></div>
                <div class="col-sm-6 col-md-4"><a href="" class="category property">Immobilien</a></div>
                <div class="col-sm-6 col-md-4"><a href="" class="category metals">Edelmetalle</a></div>
                <div class="col-xs-12">
                    <hr class="separate">
                </div>
            </div>
            <!--/.row-->
            <section class="latest-articles">
                <header>
                    <h2>Aktuelle Informationen</h2>
                </header>
                <artilce class="latest-article">
                    <figure>
                        <p class="wreper-img"><a href=""><img src="/uploads/img1.jpg" alt="photo"></a></p>
                        <figcaption>
                            <header>
                                <h3><a href="">Cu graece aliquip prodesset usu</a></h3>
                            </header>
                            <p>Velit scripta ut eam, odio summo latine ne duo, eum purto placerat in. Nominavi indoctum
                                vim
                                ne. Ea vim essent expetendis temporibus, at mel euripidis vituperatoribus, te lobortis
                                senserit evertitur pri.</p>
                            <footer>
                                <time datetime="2012-06-19">19/06/2016</time>
                            </footer>
                        </figcaption>
                    </figure>
                </artilce>
                <!--/.latest-article-->
                <artilce class="latest-article">
                    <figure>
                        <p class="wreper-img"><a href=""><img src="/uploads/img2.jpg" alt="photo"></a></p>
                        <figcaption>
                            <header>
                                <h3><a href="">Cu graece aliquip prodesset usu</a></h3>
                            </header>
                            <p>Velit scripta ut eam, odio summo latine ne duo, eum purto placerat in. Nominavi indoctum
                                vim
                                ne. Ea vim essent expetendis temporibus, at mel euripidis vituperatoribus, te lobortis
                                senserit evertitur pri.</p>
                            <footer>
                                <time datetime="2012-06-19">19/06/2016</time>
                            </footer>
                        </figcaption>
                    </figure>
                </artilce>
                <!--/.latest-article-->
            </section>
            <!--/.latest-articles-->
        </div>
        <!--/.container-->

        <div class="honors">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="bxslider style-none">
                            <li><img src="/uploads/reward1.jpg"/></li>
                            <li><img src="/uploads/reward2.jpg"/></li>
                            <li><img src="/uploads/reward3.jpg"/></li>
                            <li><img src="/uploads/reward4.jpg"/></li>
                            <li><img src="/uploads/reward5.jpg"/></li>
                            <li><img src="/uploads/reward1.jpg"/></li>
                            <li><img src="/uploads/reward2.jpg"/></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--/.honors-->

        <section class="last-reviews container">
            <div class="row">
                <div class="col-xs-12">
                    <header>
                        <h2>Bewertungen</h2>
                    </header>
                    <article class="last-review">
                        <figure>
                            <p class="wreper-img"><a href=""><img src="/uploads/review1.jpg" alt="review" width="133px"
                                                                  height="133px"></a>
                            </p>
                            <figcaption>
                                <header>
                                    <h3><a href="">Haftpflichtversicherung</a></h3>
                                </header>
                                <p>Wenn Sie gern eine Kundeneinschätzung zum Privathaftpflicht Vergleichsrechner
                                    möchten,
                                    dann orientieren Sie sich doch direkt an Bewertungen der CHECK24-Kunden-
                                    übersichtlich
                                    und einfach dargestellt. Zusätzlich können Sie auch anhand von User-Kommentaren
                                    direkt
                                    sehen, was unseren Kunden gefallen hat - und womit Sie weniger zufrieden waren.</p>
                            </figcaption>
                        </figure>
                    </article>
                    <!--/.last-review-->
                    <article class="last-review">
                        <figure>
                            <p class="wreper-img"><a href=""><img src="/uploads/review2.jpg" alt="review" width="133px"
                                                                  height="133px"></a>
                            </p>
                            <figcaption>
                                <header>
                                    <h3><a href="">Haftpflichtversicherung</a></h3>
                                </header>
                                <p>Sie möchten ehrliche und ungeschönte Kundenmeinungen einsehen, um sich ein objektives
                                    Bild vom Hundehaftpflicht Vergleichsrechner machen zu können? Wir möchten Ihnen
                                    diese
                                    Möglichkeit gern geben: Hier finden Sie alle Bewertungen der CHECK24-Kunden zum
                                    Hundehaftpflicht Vergleich. </p>
                            </figcaption>
                        </figure>
                    </article>
                    <!--/.last-review-->
                    <article class="last-review">
                        <figure>
                            <p class="wreper-img"><a href=""><img src="/uploads/review3.jpg" alt="review" width="133px"
                                                                  height="133px"></a>
                            </p>
                            <figcaption>
                                <header>
                                    <h3><a href="">Pferdehalterversicherung</a></h3>
                                </header>
                                <p>Bei CHECK24 finden Sie die für Sie individuell günstigste Pferdeversicherung. Wir
                                    durchleuchten alle Tarife und ermitteln für Sie den individuell besten. Um Ihnen die
                                    Chance zu geben, auch etwas über uns zu erfahren, lassen wir uns von unseren Kunden
                                    bewerten. Hier finden Sie alle Erfahrungen der CHECK24-Kunden mit unserem
                                    Pferdehaftpflicht Vergleichsrechner.</p>
                            </figcaption>
                        </figure>
                    </article>
                    <!--/.last-review-->
                    <div class="clear"></div>
                    <footer>
                        <p><a href="`" class="btn2">Alle Bewertungen</a></p>
                    </footer>
                    <hr class="separate">
                </div>
            </div>
        </section>
        <!--/.last-reviews-->

        <section class="additionl-info container">
            <div class="row">
                <div class="col-xs-12">
                    <header>
                        <h2>Aktuelle Informationen auf den Punkt gebracht</h2>
                    </header>
                    <p>Wir halten Sie stets über wichtige Informationen rund um unser breites Produktangebot auf dem
                        Laufenden. Ob zu den Schwerpunkten Versicherung, Finanzen, Energie, Telekommunikation, Reisen
                        oder
                        Elektronik: Unsere Experten aus der CHECK24-Redaktion sind immer nah am Geschehen und berichten
                        für
                        Sie in den News aktuell über die derzeitigen Marktentwicklungen und spannende Produktneuheiten
                        sowie
                        Trends. Außerdem werden entscheidende politische Rahmenbedingungen und weitere
                        verbraucherrelevante
                        Themen beleuchtet.</p>
                    <p>Über unsere Vergleichsrechner finden Sie passende Versicherungspolicen attraktive Stromtarife und
                        zinsgünstige Kredite einfach und sicher. Oder soll es eine schnellere Internetverbindung, ein
                        neues
                        Mobiltelefon oder die langersehnte Traumreise sein? Auch diese Wünsche gehen mit CHECK24 in
                        Erfüllung. Für die Servicequalität, Benutzerfreundlichkeit und die konsequenten
                        Datenschutzrichtlinien ist CHECK24 vielfach ausgezeichnet.</p>
                </div>
            </div>
        </section>
        <!--/.adiitinal-info-->
    </main>

<?php
get_footer();
