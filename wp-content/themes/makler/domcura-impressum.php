<?php
/*
Template Name: domcura-impressum
*/
get_header('domcura');
?>

<section class="impressum-header">
      <div class="impressum-header__slide" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/impressum-header.jpg')">
        <div class="container-new">
          <h4><?php
              $post_id = get_post( 1380, ARRAY_A);
              $content = $post_id['post_title'];
              echo ($content);
              ?></h4>
        </div>
      </div>
      <div class="container-new impressum-content">
        <!-- <div class="impressum-content__achtung"> -->
        <?php
        $post_id = get_post( 1380, ARRAY_A);
        $content = $post_id['post_content'];
        echo ($content);
        ?>
        <!-- </div> -->
      </div>
    </section>


<?php get_footer('domcura'); ?>