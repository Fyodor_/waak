<?php
/**
 * makler functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package makler
 */

if ( ! function_exists( 'makler_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function makler_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on makler, use a find and replace
	 * to change 'makler' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'makler', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'makler' ),
		'footer' => esc_html__( 'Footer menu', 'makler' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'makler_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'makler_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function makler_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'makler_content_width', 640 );
}
add_action( 'after_setup_theme', 'makler_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function makler_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'makler' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'makler' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'makler_widgets_init' );

/**
 * Enqueue scripts and styles.
 */

function makler_scripts() {
  if ( is_page_template(array('docura.php', 'docura-product.php', 'docura-impressum.php', 'docura-datenschutz.php', 'docura-agb.php', 'nrv.php', 'nrv-product.php', 'nrv-impressum.php', 'nrv-datenschutz.php', 'nrv-agb.php','domcura.php','domcura-product.php', 'domcura-impressum.php', 'domcura-datenschutz.php', 'domcura-agb.php', 'deurag.php', 'deurag-product.php', 'deurag-impressum.php', 'deurag-datenschutz.php', 'deurag-agb.php'))) {
    //здесь подключите свои стили
    } else {
    wp_enqueue_style( 'makler-style', get_stylesheet_uri(), array('my_style') );

	wp_enqueue_script( 'makler-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'makler-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
    }
	
}
add_action( 'wp_enqueue_scripts', 'makler_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

//подключение пользовательских скриптов

function my_scripts()
{
  add_action( 'wp_enqueue_scripts', 'my_scripts_method' );
  if ( is_page_template(array('docura.php', 'docura-product.php', 'docura-impressum.php', 'docura-datenschutz.php', 'docura-agb.php', 'nrv.php', 'nrv-product.php', 'nrv-impressum.php', 'nrv-datenschutz.php', 'nrv-agb.php','domcura.php','domcura-product.php', 'domcura-impressum.php', 'domcura-datenschutz.php', 'domcura-agb.php', 'deurag.php', 'deurag-product.php', 'deurag-impressum.php', 'deurag-datenschutz.php', 'deurag-agb.php'))) {
    
    wp_enqueue_script('slick', get_template_directory_uri() . '/js/slick.min.js', array( 'jquery'), '1', true);
    wp_enqueue_script('relax', get_template_directory_uri() . '/js/relax/rellax.min.js', array( 'jquery'), '1', true);
    wp_enqueue_script('html5shiv', get_template_directory_uri() . '/js/html5shiv/html5shiv.min.js', array( 'jquery'), '1', true);
    wp_enqueue_script('calculator', 'https://www.mr-money.de/iframeControls.js', array( 'jquery'), '1', true);
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery'), '3.3.5', true);
    wp_enqueue_script('docura_main', get_template_directory_uri() . '/js/main.docura-min.js', array( 'jquery'), '1', true);
    if ( is_user_logged_in() ) {
      ?>
        <script>
        document.addEventListener("DOMContentLoaded", ready);
        function ready() {
          document.getElementsByClassName('docura-header')[0].style.marginTop = '32px';
        }
        </script>
      <?php
    }
    if(is_page(1398)){
      wp_enqueue_script('textarea_autosize', get_template_directory_uri() . '/template-parts/custom-calculator/custom-calculator.js', array( 'jquery'), '1', true);
    }
  } else {
    // Register the script like this for a theme:
	wp_register_script( 'my-script', get_template_directory_uri() . '/js/main.min.js', array( 'jquery'), '1', true );
	// For either a plugin or a theme, you can then enqueue the script:
	wp_enqueue_script( 'my-script' );

	// Register the script like this for a theme:
	wp_register_script( 'add-script', get_template_directory_uri() . '/js/scripts.js', array( 'jquery','my-script'), '1', true );
	// For either a plugin or a theme, you can then enqueue the script:
	wp_enqueue_script( 'add-script' );

	// Register the script like this for a theme:
	//wp_register_script( 'add-script', get_template_directory_uri() . '/js/add.js', array( 'jquery'), '1', true );
	// For either a plugin or a theme, you can then enqueue the script:
	//wp_enqueue_script( 'add-script' );
    }
}
add_action( 'wp_enqueue_scripts', 'my_scripts', 100 );

function my_styles()
{
  if ( is_page_template(array('docura.php', 'docura-product.php', 'docura-impressum.php', 'docura-datenschutz.php', 'docura-agb.php', 'nrv.php', 'nrv-product.php', 'nrv-impressum.php', 'nrv-datenschutz.php', 'nrv-agb.php','domcura.php','domcura-product.php', 'domcura-impressum.php', 'domcura-datenschutz.php', 'domcura-agb.php', 'deurag.php', 'deurag-product.php', 'deurag-impressum.php', 'deurag-datenschutz.php', 'deurag-agb.php'))) {
    wp_register_style( 'libs', get_template_directory_uri() . '/css/docura.libs.min.css', array(), '1.0', 'all' );
    wp_enqueue_style( 'libs' );
    wp_register_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.3.5', 'all' );
    wp_enqueue_style( 'bootstrap' );
    wp_register_style( 'my_style', get_template_directory_uri() . '/css/main.docura-min.css', array(), '1.0', 'all' );
    wp_enqueue_style( 'my_style' );
    if(is_page(1398)){
      wp_register_style( 'style_for_calculator', get_template_directory_uri() . '/template-parts/custom-calculator/custom-calculator.css', array(), '1.0', 'all' );
      wp_enqueue_style( 'style_for_calculator' );
    }
    } else {
// Register the style like this for a theme:
	wp_register_style( 'my_style', get_template_directory_uri() . '/css/main.min.css', array(), '1', 'all' );
	// For either a plugin or a theme, you can then enqueue the style:
	wp_enqueue_style( 'my_style' );
    }

	
}
add_action( 'wp_enqueue_scripts', 'my_styles' );

// Create Main Slider Post Type
require_once( get_template_directory() . '/inc/main-slider/slider_post_type.php' );
// Create Main Slider
require_once( get_template_directory() . '/inc/main-slider/slider.php' );

// Create Honors Slider Post Type
require_once( get_template_directory() . '/inc/slider/slider_post_type.php' );
// Create Honors Slider
require_once( get_template_directory() . '/inc/slider/slider.php' );

// Create Review Post Type
require_once( get_template_directory() . '/inc/reviews-type/review_post_type.php' );

//Определим использование миниатюр в шаблоне и укажем размеры миниатюры поста
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 160, 160, true );
}

//изменение [...] через фильтр excerpt_more
add_filter('excerpt_more', function($more) {
	return '...';
});
//Удаляем category из УРЛа категорий
add_filter('category_link', create_function('$a', 'return str_replace("category/", "", $a);'), 9999);



// add_action( 'init', 'register_post_types' );
// function register_post_types(){
// 	register_post_type('post_type_name', array(
// 		'label'  => null,
// 		'labels' => array(
// 			'name'               => 'docura', // основное название для типа записи
// 			'singular_name'      => 'page-part', // название для одной записи этого типа
// 			'add_new'            => 'Add page-part', // для добавления новой записи
// 			'add_new_item'       => 'Добавление ____', // заголовка у вновь создаваемой записи в админ-панели.
// 			'edit_item'          => 'Редактирование ____', // для редактирования типа записи
// 			'new_item'           => 'Новое ____', // текст новой записи
// 			'view_item'          => 'Смотреть ____', // для просмотра записи этого типа.
// 			'search_items'       => 'Искать ____', // для поиска по этим типам записи
// 			'not_found'          => 'Не найдено', // если в результате поиска ничего не было найдено
// 			'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
// 			'parent_item_colon'  => '', // для родителей (у древовидных типов)
// 			'menu_name'          => 'docura', // название меню
// 		),
// 		'description'         => '',
// 		'public'              => true,
// 		'publicly_queryable'  => null, // зависит от public
// 		'exclude_from_search' => null, // зависит от public
// 		'show_ui'             => null, // зависит от public
// 		'show_in_menu'        => null, // показывать ли в меню адмнки
// 		'show_in_admin_bar'   => null, // по умолчанию значение show_in_menu
// 		'show_in_nav_menus'   => null, // зависит от public
// 		'show_in_rest'        => null, // добавить в REST API. C WP 4.7
// 		'rest_base'           => null, // $post_type. C WP 4.7
// 		'menu_position'       => null,
// 		'menu_icon'           => null, 
// 		//'capability_type'   => 'post',
// 		//'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
// 		//'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
// 		'hierarchical'        => false,
// 		'supports'            => array('title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
// 		'taxonomies'          => array(),
// 		'has_archive'         => false,
// 		'rewrite'             => true,
// 		'query_var'           => true,
//   ) );
  
//   register_taxonomy('register_post_types', 'post_type_name', array(
// 		'label'                 => 'types', // определяется параметром $labels->name
// 		'labels'                => array(
// 			'name'              => 'Genres',
// 			'singular_name'     => 'Genre',
// 			'search_items'      => 'Search Genres',
// 			'all_items'         => 'All Genres',
// 			'view_item '        => 'View Genre',
// 			'parent_item'       => 'Parent Genre',
// 			'parent_item_colon' => 'Parent Genre:',
// 			'edit_item'         => 'Edit Genre',
// 			'update_item'       => 'Update Genre',
// 			'add_new_item'      => 'Add New Genre',
// 			'new_item_name'     => 'New Genre Name',
// 			'menu_name'         => 'Genre',
// 		),
// 		'description'           => 'description', // описание таксономии
// 		'public'                => true,
// 		'publicly_queryable'    => null, // равен аргументу public
// 		'show_in_nav_menus'     => true, // равен аргументу public
// 		'show_ui'               => true, // равен аргументу public
// 		'show_in_menu'          => true, // равен аргументу show_ui
// 		'show_tagcloud'         => true, // равен аргументу show_ui
// 		'show_in_rest'          => null, // добавить в REST API
// 		'rest_base'             => null, // $taxonomy
// 		'hierarchical'          => true,
// 		'update_count_callback' => '',
// 		'rewrite'               => true,
// 		//'query_var'             => $taxonomy, // название параметра запроса
// 		'capabilities'          => array(),
// 		'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
// 		'show_admin_column'     => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
// 		'_builtin'              => false,
// 		'show_in_quick_edit'    => null, // по умолчанию значение show_ui
// 	) );
// }


#carbon fields
require_once( get_template_directory() . '/inc/carbon-fields/carbon-fields-plugin.php' );

add_action( 'carbon_fields_register_fields', 'crb_register_custom_fields' ); // Для версии 2.0 и выше
//add_action( 'carbon_register_fields', 'crb_register_custom_fields' ); // Для версии 1.6 и ниже
function crb_register_custom_fields() {
	// путь к пользовательскому файлу определения поля (полей), измените под себя
	require_once __DIR__ . '/inc/custom-fields/post-meta.php';
}


# redirect from http to https
add_action('init', 'redirect_http_to_https');
function redirect_http_to_https(){
    if( is_ssl() ) return;

    if ( 0 === strpos($_SERVER['REQUEST_URI'], 'http') )
        wp_redirect( set_url_scheme( $_SERVER['REQUEST_URI'], 'https' ), 301 );
    else
        wp_redirect( 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], 301 );
    exit;
}