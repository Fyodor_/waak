<footer class="docura-footer nrv">
      <div class="container-new">
        <div class="footer__main">
          <div class="footer__logos">
            <div class="logos nrv"><a href="<?php echo get_post_meta('1281', 'logo', true); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/nrv/nrv-small-logo.png" alt="docura-logo"></a><a class="icon docura-icon-waak-small-logo" href="/"></a></div><a class="phone" href="tel:<?php echo get_post_meta('1281', 'phone', true); ?>"><?php echo get_post_meta('1281', 'phone', true); ?></a><a class="mail" href="mailto:<?php echo get_post_meta('1281', 'email', true); ?>"><?php echo get_post_meta('1281', 'email', true); ?></a>
          </div>
          <div class="footer-links-wrapper">
            <div class="footer__products">
              <ul>
                <?php
$page_children = new WP_Query(array(
    'post_type' => 'page',
    'post_parent' => '1286',
)
);
if ($page_children->have_posts()):
    while ($page_children->have_posts()): $page_children->the_post();
        $title = get_the_title();
        echo '<li><a href="' . get_the_permalink() . '">' . $title . '</a></li>';
    endwhile;
endif;
wp_reset_query();
?>
              </ul>
            </div>
            <div class="footer__legal">
              <ul>
                <li><a href="<?php echo get_option('home'); ?>/nrv/impressum">Impessum</a></li>
                <li><a href="<?php echo get_option('home'); ?>/nrv/datenschutz">Datenschutz</a></li>
                <li><a href="<?php echo get_option('home'); ?>/nrv/agb">AGB</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <hr class="hr-docura">
      <div class="container-new">
        <div class="footer__year">© 2018 NRV | Waak</div>
      </div>
    </footer>

    <div class="container-new">
      <div class="nav-icon4 nav-burger"><span></span><span></span><span></span></div>
    </div>
    <div class="hiddenone visuallyhiddenone" id="burgerDocuraPopupProduct">
      <div class="container-new">
        <header>
          <div class="burger-icons-wrapper"><a class="burger-logo nrv" href="<?php echo get_post_meta('1281', 'docura_logo', true); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/nrv/nrv-small-logo.png" alt="docura-logo"></a><a class="icon docura-icon-waak-small-logo" href="#"></a></div>
          <div class="phone">
            <div class="icon nrv-icon-phone"></div><a href="tel:<?php echo get_post_meta('1281', 'phone', true); ?>"><?php echo get_post_meta('1281', 'phone', true); ?></a>
          </div>
        </header>
        <div class="burger-content nrv">
          <ul>
            <li><a href="#vorteile">Vorteile</a></li>
            <li><a href="#wieman1">Drei Schritte</a></li>
            <li><a href="#calculator">Online Abschließen</a></li>
            <li><a href="#falle">Fälle</a></li>
            <li><a href="#faq">FAQ</a></li>
            <li><a class="producte" href="#produkte">Producte</a><img src="<?php echo get_template_directory_uri(); ?>/img/icons/header-arrow.svg" alt="header-arrow">
              <div class="submenu-wrapper">
                <ul>
                <?php
$page_children = new WP_Query(array(
    'post_type' => 'page',
    'post_parent' => '1286',
)
);
if ($page_children->have_posts()):
    while ($page_children->have_posts()): $page_children->the_post();
        $title = get_the_title();
        echo '<li><a href="' . get_the_permalink() . '">' . $title . '</a></li>';
    endwhile;
endif;
wp_reset_query();
?>
                </ul>
              </div>
            </li>
          </ul>
          <div class="phone phone-footer">
            <div class="icon nrv-icon-phone"></div><a href="tel:<?php echo get_post_meta('1281', 'phone', true); ?>"><?php echo get_post_meta('1281', 'phone', true); ?></a>
          </div>
        </div>
      </div>
    </div>
    <?php wp_footer();?>
  </body>
</html>






