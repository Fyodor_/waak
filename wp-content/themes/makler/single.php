<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package makler
 */

get_header(); ?>

	<div id="primary" class="content-area">
<?php echo get_post_type(1028); ?>
		<?php
    while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', get_post_format() );
			$args = array(
				'screen_reader_text' => __( ' ' ),
				'next_text' => '<span class="post-title"><i class="glyphicon glyphicon-menu-right pull-right"></i>%title</span>',
				'prev_text' => '<span class="post-title"><i class="glyphicon glyphicon-menu-left pull-left"></i>%title</span>'
			);?>

			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<?php the_post_navigation($args); ?>
					</div>
				</div>
			</div>

		<?php endwhile; // End of the loop.
		?>

	</div><!-- #primary -->

<?php
get_footer();
