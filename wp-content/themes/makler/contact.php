<?php
/*
Template Name: Contacts
*/
get_header();
?>

<div class="main">
    <?php /*get_template_part( 'slider' )*/ ?>

    <div class="container addedContainer">
        <div class="addedContainerInner">
            <div class="breadcrumbs">
                <?php
                if(function_exists('bcn_display'))
                {
                    bcn_display();
                }
                ?>
            </div>
            <div class="contact">
                <h2><?= the_title(); ?></h2>
                <div class="row regFormAndPhoto">
                    <div class="col-xs-8 col-sm-7">
                        <p><?php esc_html_e( 'Wir freuen uns auf Ihre (kostenlose und unverbindliche) Anfrage und werden uns umgehend bei Ihnen melden!', 'makler' ); ?></p>
                        <?= do_shortcode('[contact-form-7 id="54" title="Contact form 1"]') ?>
                    </div>
                    <div class="col-xs-4 col-sm-offset-1">
                        <div class="photoDiv">
                            <?php
                            $thumb_id = get_post_thumbnail_id();
                            $thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
                            ?>
                            <img src="<?= $thumb_url[0]  ?>" class="img-responsive" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>