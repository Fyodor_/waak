<?php
/** Create Slider */
function bxslider_template()
{
// Query Arguments
    $args = array(
        'post_type' => 'slides',
        'posts_per_page' => -1
    );
// The Query
    $the_query = new WP_Query($args);
// Check if the Query returns any posts
    if ($the_query->have_posts()) {
// Start the Slider ?>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="bxslider style-none">
                        <?php
                        // The Loop
                        while ($the_query->have_posts()) : $the_query->the_post(); ?>
                            <li>
                                <?php // Check if there's a Slide URL given and if so let's a link to it
                                    // The Slide's Image
                                    echo the_post_thumbnail();
                                    // Close off the Slide's Link if there is one
                                    ?>
                            </li>
                        <?php endwhile; ?>
                    </ul><!-- .bxslides -->
                </div><!-- .container -->
            </div><!-- .row -->
        </div><!-- .col-xs-12 -->
    <?php }
// Reset Post Data
    wp_reset_postdata();
}