<?php
/** Create Slider */
function mainslider_template()
{
// Query Arguments
    $args = array(
        'post_type' => 'main_slides',
        'posts_per_page' => -1
    );
// The Query
    $the_query = new WP_Query($args);
// Check if the Query returns any posts
    if ($the_query->have_posts()) {
// Start the Slider ?>
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <?php
            $count_li = 0;
            // The Loop
            while ($the_query->have_posts()) :
                $the_query->the_post();
                if ($count_li == 0) : ?>
                    <li data-target="#carousel-example-generic" data-slide-to="<?= $count_li ?>" class="active"></li>
                <?php else : ?>
                    <li data-target="#carousel-example-generic" data-slide-to="<?= $count_li ?>"></li>
                <?php endif; ?>
                <?php $count_li++ ?>
            <?php endwhile; ?>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <?php
            $count = 0;
            // The Loop
            while ($the_query->have_posts()) :
            $the_query->the_post();
            if ($count == 0) : ?>
            <div class="item active">
                <?php else : ?>
                <div class="item">
                    <?php endif; ?>
                    <?php the_post_thumbnail('full', array(
                        'class' => 'img-responsive',
                        'alt' => trim(strip_tags(get_the_title()))
                    )); ?>
                    <div class="carousel-caption">
                        <h3><?php the_title(); ?></h3>
                        <p><?php the_content(); ?></p>
                        <p class="btn-wreper"><a href="<?php the_field('url'); ?>" target="_blank"
                                                 class="btn1"><?php esc_html_e('Mehr Lesen', 'makler') ?></a>
                        </p>
                    </div>
                </div>
                <?php $count++ ?>
                <?php endwhile; ?>

            </div>

        </div>
        <!--/#carousel-example-generic-->
    <?php }
// Reset Post Data
    wp_reset_postdata();
}