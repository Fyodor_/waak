<?php
// Create Custom Post Type Review
function review_posttype() {
    $labels = array(
        'name'              => _x( 'Reviews', 'post type general name' ),
        'singular_name'     => _x( 'Reviews', 'post type singular name' ),
        'add_new'           => __( 'Add New Review' ),
        'add_new_item'      => __( 'Add New Review' ),
        'edit_item'         => __( 'Edit Review' ),
        'new_item'          => __( 'New Review' ),
        'view_item'         => __( 'View Review' ),
        'search_items'      => __( 'Search Reviews' ),
        'not_found'         => __( 'Review' ),
        'not_found_in_trash'=> __( 'Review' ),
        'parent_item_colon' => __( 'Review' ),
        'menu_name'         => __( 'Reviews' )
    );
    $taxonomies = array();
    $supports = array('title','editor','thumbnail');
    $post_type_args = array(
        'labels'            => $labels,
        'singular_label'    => __('Review'),
        'public'            => true,
        'show_ui'           => true,
        'publicly_queryable'=> true,
        'query_var'         => true,
        'capability_type'   => 'post',
        'has_archive'       => false,
        'hierarchical'      => false,
        'rewrite'           => array( 'slug' => 'reviews', 'with_front' => false ),
        'supports'          => $supports,
        'menu_position'     => 27, // Where it is in the menu. Change to 6 and it's below posts. 11 and it's below media, etc.
        'taxonomies'        => $taxonomies
    );
    register_post_type('reviews',$post_type_args);
}
add_action('init', 'review_posttype');