<?php
use Carbon_Fields\Container;
use Carbon_Fields\Field;



Container::make('post_meta', 'sertificate')
  ->where( 'post_parent_id', '=', '1034' )
  ->or_where( 'post_parent_id', '=', '1286')
  ->or_where( 'post_parent_id', '=', '1319')
  ->or_where( 'post_parent_id', '=', '1393')
  ->add_fields(array(
    Field::make( 'complex', 'sertificate', 'sertificate' )
    ->add_fields( array(
      Field::make( 'image', 'sertificate', 'put sertificate images' ),
    )
)
//->help_text( 'Optionale Zusatzleistungen für die Privat haftpflicht­versicherung - add - remove' )
));

Container::make('post_meta', 'vorteile header')
  ->where( 'post_parent_id', '=', '1034' )
  ->or_where( 'post_parent_id', '=', '1286')
  ->or_where( 'post_parent_id', '=', '1319')
  ->or_where( 'post_parent_id', '=', '1393')
	->add_fields(array(
		Field::make('text', 'vorteile_header'),
  ));

Container::make( 'post_meta', 'Ihre Vorteile im Überblick' )
->where( 'post_parent_id', '=', '1034' )
->or_where( 'post_parent_id', '=', '1286')
->or_where( 'post_parent_id', '=', '1319')
->or_where( 'post_parent_id', '=', '1393')
		->add_fields( array(
						Field::make( 'complex', 'vorteile', 'vorteile' )
							->add_fields( array(
								Field::make( 'text', 'vorteile', 'vorteile' )
									->set_width( 33 ),
							)
							)
							->help_text( 'Add vorteiles' )
) );

Container::make('post_meta', 'vorteile files')
->where( 'post_parent_id', '=', '1034' )
->or_where( 'post_parent_id', '=', '1286')
->or_where( 'post_parent_id', '=', '1319')
->or_where( 'post_parent_id', '=', '1393')
  ->add_fields(array(
    Field::make( 'complex', 'vorteile_files', 'vorteile_files' )
    ->add_fields( array(
      Field::make('file', 'vorteile_file', 'vorteile_file'),
      Field::make( 'text', 'vorteile_file_name', 'vorteile_file_name' )
    )
)));

Container::make('post_meta', 'Nur noch drei Schritte zu Ihrem Schutz')
->where( 'post_parent_id', '=', '1034' )
->or_where( 'post_parent_id', '=', '1286')
->or_where( 'post_parent_id', '=', '1319')
->or_where( 'post_parent_id', '=', '1393')
	->add_fields(array(
    Field::make( 'text', 'nur_header', 'nur_header' ),
    Field::make( 'text', 'nur_sub_header', 'nur_sub_header' ),
		Field::make('image', 'nur-image-1'),
		Field::make('text', 'nur-text-1'),
    Field::make('image', 'nur-image-2'),
		Field::make('text', 'nur-text-2'),
    Field::make('image', 'nur-image-3'),
		Field::make('text', 'nur-text-3'),
  ));

Container::make('post_meta', 'Warum die private Haftpflichtv ersiche rung so wichtig ist')
->where( 'post_parent_id', '=', '1034' )
->or_where( 'post_parent_id', '=', '1286')
->or_where( 'post_parent_id', '=', '1319')
->or_where( 'post_parent_id', '=', '1393')
	->add_fields(array(
    Field::make( 'text', 'warum_header', 'warum_header' ),
		Field::make('textarea', 'warum-text'),
));

Container::make('post_meta', 'in_diesen')
->where( 'post_parent_id', '=', '1034' )
->or_where( 'post_parent_id', '=', '1286')
->or_where( 'post_parent_id', '=', '1319')
->or_where( 'post_parent_id', '=', '1393')
  ->add_fields(array(
    Field::make( 'complex', 'in_diesen', 'In diesen Fällen ist eine Haftpflicht­versicherung von Vorteil' )
    ->add_fields( array(
      Field::make( 'text', 'in_diesen_number', 'put unicual for this accordeon id' ),
      Field::make( 'text', 'in_diesen_header', 'in_diesen_header' ),
      Field::make( 'textarea', 'in_diesen_text', 'in_diesen_text' )
    )
)
->help_text( 'In diesen Fällen ist eine Haftpflicht­versicherung von Vorteil - add - remove' )
));

Container::make('post_meta', 'optionale-header')
->where( 'post_parent_id', '=', '1034' )
->or_where( 'post_parent_id', '=', '1286')
->or_where( 'post_parent_id', '=', '1319')
->or_where( 'post_parent_id', '=', '1393')
	->add_fields(array(
		Field::make('textarea', 'optionale-header', 'optionale-header'),
		Field::make('textarea', 'optionale-subheader', 'optionale-subheader')
));

Container::make('post_meta', 'optionale')
->where( 'post_parent_id', '=', '1034' )
->or_where( 'post_parent_id', '=', '1286')
->or_where( 'post_parent_id', '=', '1319')
->or_where( 'post_parent_id', '=', '1393')
  ->add_fields(array(
    Field::make( 'complex', 'optionale', 'Optionale Zusatzleistungen für die Privat haftpflicht­versicherung' )
    ->add_fields( array(
      Field::make( 'text', 'optionale_number', 'put unicual for this accordeon id' ),
      Field::make( 'text', 'optionale_header', 'optionale_header' ),
      Field::make( 'textarea', 'optionale_text', 'optionale_text' )
    )
)
->help_text( 'Optionale Zusatzleistungen für die Privat haftpflicht­versicherung - add - remove' )
));

Container::make('post_meta', 'haufig')
->where( 'post_parent_id', '=', '1034' )
->or_where( 'post_parent_id', '=', '1286')
->or_where( 'post_parent_id', '=', '1319')
->or_where( 'post_parent_id', '=', '1393')
  ->add_fields(array(
    Field::make( 'complex', 'haufig', 'Häufig gestellte Fragen' )
    ->add_fields( array(
      Field::make( 'text', 'haufig_number', 'put unicual for this accordeon id' ),
      Field::make( 'text', 'haufig_header', 'haufig_header' ),
      Field::make( 'rich_text', 'haufig_text', 'haufig_text' )
    )
)
->help_text( 'Häufig gestellte Fragen - add - remove' )
));

Container::make('post_meta', 'calculator')
->where( 'post_parent_id', '=', '1034' )
->or_where( 'post_parent_id', '=', '1286')
->or_where( 'post_parent_id', '=', '1319')
->or_where( 'post_parent_id', '=', '1393')
	->add_fields(array(
		Field::make('textarea', 'header-calc', 'calculator header'),
		Field::make('textarea', 'small-calc', 'small calculator all text'),
		Field::make('textarea', 'big-calc', 'big calculator src'),
));

Container::make('post_meta', 'custom-calculator-subheader')
->where( 'post_id', '=', '1398' )
	->add_fields(array(
		Field::make('textarea', 'subheader-calc', 'calculator subheader'),
));

Container::make('post_meta', 'in_diesen-header')
->where( 'post_parent_id', '=', '1034' )
->or_where( 'post_parent_id', '=', '1286')
->or_where( 'post_parent_id', '=', '1319')
->or_where( 'post_parent_id', '=', '1393')
	->add_fields(array(
		Field::make('text', 'in_diesen-header', 'in_diesen-header')
));

Container::make('post_meta', 'haufig-header')
->where( 'post_parent_id', '=', '1034' )
->or_where( 'post_parent_id', '=', '1286')
->or_where( 'post_parent_id', '=', '1319')
->or_where( 'post_parent_id', '=', '1393')
	->add_fields(array(
		Field::make('text', 'haufig-header', 'haufig-header')
));