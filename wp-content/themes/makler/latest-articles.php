<section class="latest-articles">
    <header>
        <h2><?php esc_html_e('Aktuelle Informationen', 'makler') ?></h2>
    </header>
    <?php // Query Arguments
    $args = array(
        'post_type' => 'post',
        'category_name' => 'aktuelle',
        'posts_per_page' => 2
    );
    // The Query
    $the_query = new WP_Query($args);
    while ($the_query->have_posts()) : $the_query->the_post(); ?>
        <artilce class="latest-article">
            <figure>
                <p class="wreper-img"><a href="<?php the_permalink(); ?>">
                        <?php the_post_thumbnail('thumbnail', array(
                            'class' => 'img-responsive',
                            'alt' => trim(strip_tags(get_the_title()))
                        )); ?>
                        <!--                    <img src="/uploads/img1.jpg" alt="photo">-->
                    </a></p>
                <figcaption>
                    <header>
                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    </header>
                    <?php the_excerpt(); ?>
                    <footer>
                        <time datetime="<?php the_time('Y-m-d'); ?>"><?php the_time('d/m/Y'); ?></time>
                    </footer>
                </figcaption>
            </figure>
        </artilce>
        <!--/.latest-article-->
    <?php endwhile; ?>
    <?php wp_reset_postdata(); ?>
    <p class="all-articles"><a href="/blog" class="btn1">Weitere News</a></p>
</section>
<!--/.latest-articles-->