<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package makler
 */

get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <?php /*get_template_part('slider')*/ ?>
            <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <header class="page-header">
                                <h1><?php single_cat_title(); ?></h1>
                            </header><!-- .page-header -->
                        </div>
                    </div>

                    <div class="row">
                        <?php
                        $args = array(
                            'numberposts' => -1,
                            'category__in' => '3'
                        );
                        $posts = new WP_Query($args);
                        while ($posts->have_posts()) : $posts->the_post(); ?>
                            <artilce class="latest-article">
                                <figure>
                                    <p class="wreper-img"><a href="<?php the_permalink(); ?>">
                                            <?php
                                            if (has_post_thumbnail()) :
                                                the_post_thumbnail('thumbnail', array(
                                                    'class' => 'img-responsive',
                                                    'alt' => trim(strip_tags(get_the_title()))
                                                ));
                                            else:
                                                echo '<img src="' . get_template_directory_uri() . '/img/no_photo1.jpg" alt="No image" class="img-responsive" height="160" width="160">';
                                            endif; ?>
                                            <!--                    <img src="/uploads/img1.jpg" alt="photo">-->
                                        </a></p>
                                    <figcaption>
                                        <header>
                                            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                        </header>
                                        <?php the_excerpt(); ?>
                                        <footer>
                                            <time
                                                datetime="<?php the_time('Y-m-d'); ?>"><?php the_time('d/m/Y'); ?></time>
                                        </footer>
                                    </figcaption>
                                </figure>
                            </artilce>
                            <!--/.latest-article-->
                        <?php endwhile; ?>
                        <?php wp_reset_postdata();


                        /*else :

                            get_template_part('template-parts/content', 'none');

                        endif;*/ ?>
                    </div>

                    <?php


                ?>
            </div>


        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();
