<?php
/*
Template Name: Blog
*/
get_header();
?>

<div class="main">
    <?php /*get_template_part('slider')*/ ?>

    <div class="container addedContainer">
        <div class="addedContainerInner">
            <div class="breadcrumbs">
                <?php
                if(function_exists('bcn_display'))
                {
                    bcn_display();
                }
                ?>
            </div>
            <div class="blog">
                <h2><?php esc_html_e('Blog', 'makler'); ?></h2>
                <div class="row">
                    <?php
                    $query = new WP_Query(array(
                        'category_name' => 'blog',
                        'posts_per_page' => -1,
                        'orderby' => 'date',
                        'order' => 'DESC'
                    ));
                    while  (  $query->have_posts()  ) :  $query->the_post();
                        ?>
                        <div class="col-lg-4 col-sm-6 postDiv preview-post">
                            <div class="postTitleDiv"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
                            <div class="authorDiv clearfix">
                                <div class="authorPhotoDiv">
                                    <?php $comment = get_the_author_meta('user_email'); ?>
                                    <?= get_avatar($comment); ?>
                                </div>
                                <div class="authorAndCategoryInfo">
                                    <span class="authorName"><?php the_author(); ?></span>
                                    <span class="postDate"><?php the_time('l, j F Y'); ?></span>
                                    <!--<span class="categoryName"><?php /*the_category(); */?></span>-->
                                </div>
                            </div>
                            <div class="postTextDiv">
                                <?php the_excerpt(); ?>
                            </div>
                            <div class="looksAndCommentsItem">
                                <div class="looksItem"><span class="sprite sprite-eye"></span><?php if(function_exists('the_views')) { the_views(); } ?></div>
                                <!--<div class="commentsItem"><span class="sprite sprite-commentIcon"></span>12</div>-->
                            </div>
                        </div>
                        <?php
                    endwhile;
                    wp_reset_postdata()
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>

