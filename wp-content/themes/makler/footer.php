<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package makler
 */

?>

</div><!-- #content -->

<footer class="main-footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <nav>
                    <?php
                    $args = array(
                        'theme_location'            => 'footer',
                        'menu_class'      => 'footer-nav',
                        'depth'           => 0
                    );
                    wp_nav_menu( $args );
                    ?>
                    <!--<ul class="footer-nav">
                        <li><a href="">Versicherungen</a></li>
                        <li><a href="">Konto & Kredit</a></li>
                        <li><a href="">Strom & Gas</a></li>
                        <li><a href="">DSL & Handy</a></li>
                        <li><a href="">Reise & Mietwagen</a></li>
                        <li><a href="">Shopping</a></li>
                        <li><a href="">Impressum</a></li>
                        <li><a href="">Datenschutz </a></li>
                    </ul>-->
                </nav>
            </div>
            <div class="copiright col-xs-12">
                <p>© 2016 Waak UG Versicherungsmakler. Alle Inhalte unterliegen unserem Copyright.</p>
            </div>
            <!-- /.copiright -->

            <div class="social-links">
                <ul class="social-links_ul style-none">
                    <li><a href="https://www.facebook.com/waakversicherungen/" class="fb" target="_blank"></a></li>
                    <li><a href="https://www.youtube.com/channel/UCiI-p5z_0oxTF2uiNn4lrdA" class="yt" target="_blank"></a></li>
                    <!--<li><a href="" class="google"></a></li>-->
                    <li><a href="https://www.xing.com/profile/Conrad_Waak2" class="xing" target="_blank"></a></li>
                </ul>
            </div>
            <!-- /.social-links -->

            <!--<div class="development">
                <p>Development by <br> <a href="http://webcapitan.com/">WebCapitan</a></p>
            </div>-->
            <!-- /.development -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->

</footer>
<!-- main-footer -->


</div><!-- #page -->

<?php wp_footer(); ?>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<script type="text/javascript" src="/reject/reject.min.js"></script>
<![endif]-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script type="text/javascript">
    WebFontConfig = {
        google: {families: ['Open+Sans:400,300,700:latin,cyrillic-ext,latin-ext']}
    };
    (function () {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
            '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    })(); </script>

</body>
</html>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-80340342-1', 'auto');
  ga('send', 'pageview');

</script>