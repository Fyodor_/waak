<?php
/*
Template Name: domcura-datenschutz
*/
get_header('domcura');
?>

    <section class="datenshutz">
      <div class="datenshutz__slide" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/datenschutz.jpg')">
        <div class="container-new">
          <h4><?php
              $post_id = get_post( 1382, ARRAY_A);
              $content = $post_id['post_title'];
              echo ($content);
              ?></h4>
        </div>
      </div>
      <div class="container-new datenshutz-content">
        <div class="datenshutz-content__datenshutz">
        <?php
        $post_id = get_post( 1382, ARRAY_A);
        $content = $post_id['post_content'];
        echo ($content);
        ?>
        </div>
      </div>
    </section>


<?php get_footer('domcura'); ?>