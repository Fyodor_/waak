<?php
/*
Template Name: docura-agb
*/
get_header('docura');
?>

    <section class="agb">
      <div class="agb__slide" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/agb.jpg')">
        <div class="container-new">
          <h4><?php
              $post_id = get_post( 1279, ARRAY_A);
              $content = $post_id['post_title'];
              echo ($content);
              ?></h4>
        </div>
      </div>
      <div class="container-new agb-content">
        <div class="agb-content__datenshutz">
            <?php
              $post_id = get_post( 1279, ARRAY_A);
              $content = $post_id['post_content'];
              echo ($content);
              ?>
        </div>
      </div>
    </section>


<?php get_footer('docura'); ?>