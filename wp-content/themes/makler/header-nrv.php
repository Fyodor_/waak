<!DOCTYPE html>
<html lang="de">
  <head>
    <title>NRV</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="main page">
    <meta name="keywords" content="words">
    <meta name="theme-color" content="#bb417d">
    <meta name="msapplication-navbutton-color" content="#bb417d">
    <meta name="apple-mobile-web-app-status-bar-style" content="#bb417d">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/fav/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/fav/favicon-16x16.png">
    <link rel="manifest" href="/img/fav/site.webmanifest">
    <link rel="mask-icon" href="/img/fav/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="/img/fav/favicon.ico">
    <meta name="msapplication-TileColor" content="#2ca688">
    <meta name="msapplication-config" content="/img/fav/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <?php wp_head(); ?>
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-104874416-1', 'auto');
    ga('send', 'pageview');
    </script>
  </head>
  <body>
    <noscript>
      <h2 class="h2-docura">Your browser does not support JavaScript!</h2>
    </noscript>
    <header class="docura-header">
      <div class="container-new">
        <div class="logos nrv"><a href="<?php echo get_post_meta('1281', 'logo', true); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/nrv/nrv-big-logo.png" alt="nrv-logo"></a><a class="icon docura-icon-waak-header-logo" href="/"></a></div>
        <nav class="nrv">
          <ul>
            <li><a href="#vorteile">Vorteile</a></li>
            <li class="product"><a href="#produkte">Produkte</a><img src="<?php echo get_template_directory_uri(); ?>/img/icons/header-arrow.svg" alt="header-arrow">
              <div class="submenu-wrapper">
                <ul>
                <?php
                $page_children = new WP_Query(array(
                  'post_type' => 'page',
                  'post_parent' => '1286'
                  )
                );
                if($page_children->have_posts()) :
                  while($page_children->have_posts()): $page_children->the_post();
                  $title = get_the_title();
                  echo '<li><a href="'.get_the_permalink().'">'. $title .'</a></li>';
                  endwhile;
                endif; 
                wp_reset_query();
                ?>
                </ul>
              </div>
            </li>
            <li><a href="#furkunden">Für Kunden</a></li>
          </ul>
        </nav>
        <div class="mail">
          <div class="icon nrv-mail-icon"></div><a href="mailto:<?php echo get_post_meta('1281', 'email', true); ?>"><?php echo get_post_meta('1281', 'email', true); ?></a>
        </div>
      </div>
    </header>

