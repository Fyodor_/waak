jQuery(document).ready(function ($) {
    $.exists(".main-nav")
    {
        $('.main-nav>li').hover(function () {
            $(this).find('.sub-menu:first').fadeIn();
        }, function () {
            $(this).find('.sub-menu:first').fadeOut();
        });
        $(' .sub-menu>li').hover(function () {
            $(this).find('.sub-menu:first').fadeIn();
        }, function () {
            $(this).find('.sub-menu:first').fadeOut();
        });
    }

    function e(e, s, i, r) {
        $.exists(".bxslider") && $(".bxslider").bxSlider({
            minSlides: 1,
            maxSlides: 5,
            slideMargin: 30,
            speed: 2e3,
            auto: e,
            controls: s,
            pager: i,
            slideWidth: r
        })
    }

    function s() {
        $(window).width() <= "500" ? $.exists(".bxslider") && e(!0, !1, !0, 0) : $(window).width() <= "600" ? $.exists(".bxslider") && e(!1, !0, !1, 0) : $.exists(".bxslider") && e(!0, !0, !1, 160)
    }

    $(window).on("load resize", s), $.exists(".alert") && setTimeout(function () {
        $(".alert").fadeOut(5e3)
    }, 3e3), $.exists(".selectpicker") && $(".selectpicker").selectpicker();
    var i = new FontFaceObserver("Open Sans", {
            weight: 300
        }),
        r = new FontFaceObserver("Open Sans", {
            weight: 400
        }),
        t = new FontFaceObserver("Open Sans", {
            weight: 600
        }),
        n = new FontFaceObserver("Open Sans", {
            weight: 700
        });
    if (Promise.all([i.check(), r.check(), t.check(), n.check()]).then(function () {
            document.documentElement.className += " fonts-loaded"
        }), $.exists("#contact-form")) {
        var o = $("#contact-form");
        o.validate({
            submitHandler: function (e) {
                e.submit()
            },
            rules: {
                name: "required",
                email: {
                    required: !0,
                    email: !0
                },
                mobile: "required"
            },
            messages: {
                name: "Please specify your name",
                email: {
                    required: "We need your email address to contact you",
                    email: "Your email address must be in the format of name@domain.com"
                },
                mobile: "This field is required to fill"
            },
            success: function (e) {
                e.closest(".form-group").removeClass("error").addClass("valid")
            },
            errorPlacement: function (e, s) {
                s.closest(".form-group").removeClass("valid").addClass("error"), e.appendTo(s.parent(".form-group"))
            }
        })
    }
}); jQuery.exists = function (e) {
    return jQuery(e).length > 0
};